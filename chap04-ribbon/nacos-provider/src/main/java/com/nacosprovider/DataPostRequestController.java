package com.nacosprovider;

import com.nacosprovider.entity.User;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class DataPostRequestController {

    @PostMapping("/postInt")
    public int postInt(int id){
        return id;
    }

    @PostMapping("/postBox")
    public String postBox(String name,Integer id){
        return "name = " + name +" id = " + id;
    }

    /**
     * @RequestBody:
     * 要求请求参数必须是在body中的
     *
     * 如果不加@RequestBody:
     * 请求可以放在parameter、body中
     * 复杂对象也是可以传的
     * @param user
     * @return
     */
    @PostMapping("/postUser")
    public User postUser(@RequestBody User user){
        return user;
    }

    @PostMapping("/postUserNoBody")
    public User postUserNoBody( User user){
        return user;
    }

    @PostMapping(value = "/postUser2",produces = {"application/json;charset=utf-8"})
    public User postUser2(@RequestBody User user){
        return user;
    }

    @PostMapping("/postIds")
    public List<Integer> postIds(@RequestBody List<Integer> ids){
        return ids;
    }

    @PostMapping("/postUsers")
    public List<User> postUsers(@RequestBody List<User> users){
        return users;
    }

    @PostMapping("/postMap")
    public Map<String,Object> postMap(@RequestBody Map<String,Object> map){
        return map;
    }

}
