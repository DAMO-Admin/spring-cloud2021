package com.nacosprovider;

import com.nacosprovider.entity.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class DataGetRequestController {

    @GetMapping("/getInt")
    public int getInt(int id){
        return id;
    }

    @GetMapping(value = "/getBox",produces = {"application/json;charset=utf-8"})
    public String getBox(String name,Integer id){
        return "name = " + name + " id = " + id;
    }

    @GetMapping("/getReuqestParam")
    public String getReuqestParam(@RequestParam(value = "rp",required = false) Integer id){
        return "ID = " + id;
    }

    @GetMapping(value = "/getUser",produces = {"application/json;charset=utf-8"})
    public User getUser(User user){
        return user;
    }

    @GetMapping("/getIds")
    public List<Long> getIds(@RequestParam List<Long> ids){
        return ids;
    }

    @GetMapping(value = "/getMap",produces = {"application/json;charset=utf-8"})
    public Map<String,Object> getMap(@RequestParam Map<String,Object> map){
        return map;
    }

    @GetMapping(value = "/getObj",produces = {"application/json;charset=utf-8"})
    public Object getObj(@RequestParam(name = "ik",required = true,defaultValue = "500") Integer id){
        return id;
    }

    @GetMapping("/getArr")
    public String[] getArr(String[] arr){
        return arr;
    }

    @GetMapping(value = "/getList",produces = {"application/json;charset=utf-8"})
    public List<String> getList(@RequestParam List<String> names){
        return names;
    }

}
