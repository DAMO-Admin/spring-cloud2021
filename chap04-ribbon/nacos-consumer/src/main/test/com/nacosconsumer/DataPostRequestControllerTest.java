package com.nacosconsumer;

import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DataPostRequestControllerTest {
    @Autowired
    RestTemplate restTemplate;

    @Test
    public void postInt(){
        MultiValueMap<String, Integer> map= new LinkedMultiValueMap<>();
        map.add("id",100);
        HttpHeaders headers = new HttpHeaders();

        HttpEntity<MultiValueMap<String, Integer>> request = new HttpEntity<>(map, headers);

        ResponseEntity<Integer> ret = restTemplate.postForEntity("http://nacos-provider/postInt",request,Integer.class);

        System.out.println(ret);


    }

    @Test
    public void postBox(){
        MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
        map.put("name", Collections.singletonList("小黑"));
        map.add("id","100");
        HttpHeaders headers = new HttpHeaders();

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

        ResponseEntity<String> ret = restTemplate.postForEntity("http://nacos-provider/postBox",request,String.class);

        System.out.println(ret.getBody());
    }

    @Test
    public void postUser(){
        User user = new User();
        user.setId(100L);
        user.setName("小火锅");
        user.setPassword("123456");

        ResponseEntity<User> ret = restTemplate.postForEntity("http://nacos-provider/postUser",user,User.class);

        System.out.println(ret.getBody());

    }

    @Test
    public void postUserNoBody(){
        MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
        map.put("name", Collections.singletonList("小黑"));
        map.add("id","100");
        map.put("password", Collections.singletonList("123456"));

        HttpHeaders headers = new HttpHeaders();

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

        ResponseEntity<User> ret = restTemplate.postForEntity("http://nacos-provider/postUserNoBody",request,User.class);

        System.out.println(ret.getBody());

    }

    @Test
    public void postUser2(){
        User user = new User();
        user.setId(100L);
        user.setName("小火锅");
        user.setPassword("123456");

        ResponseEntity<User> ret = restTemplate.postForEntity("http://nacos-provider/postUser2",user,User.class);

        System.out.println(ret.getBody());
    }

    @Test
    public void postIds(){
        List<Integer> ids = new ArrayList<>();
        ids.add(1);
        ids.add(1);
        ids.add(1);
        ids.add(1);
        ids.add(1);

        ResponseEntity<ArrayList> ret = restTemplate.postForEntity("http://nacos-provider/postIds",ids,ArrayList.class);

        System.out.println(ret.getBody());
    }

    @Test
    public void postUsers(){
        User user = new User();
        user.setId(100L);
        user.setName("小火锅");
        user.setPassword("123456");

        List<User> users = new ArrayList<>();
        users.add(user);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(JSON.toJSONString(users), headers);

        ResponseEntity<ArrayList> ret = restTemplate.postForEntity("http://nacos-provider/postUsers",request,ArrayList.class);

        List<User> retUsers = ret.getBody();

        System.out.println(retUsers);
    }

    @Test
    public void postMap(){
        Map<String,Object> params = new HashMap<>();
        params.put("id",100);
        params.put("爱好","摩托、轮滑、越野、帆船");

        Map<String,Object> ret = restTemplate.postForObject("http://nacos-provider/postMap",params,HashMap.class);

        System.out.println(ret);

    }

}
