package com.nacosconsumer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DataGetRequestControllerTest {

    @Autowired
    RestTemplate restTemplate;

    @Test
    public void getInt(){
        Map<String,Object> params = new HashMap<>();
        params.put("id",10);

        ResponseEntity<Integer> responseEntity = restTemplate.getForEntity("http://nacos-provider/getInt?id={id}",Integer.class,params);
        Integer ret = responseEntity.getBody();
        System.out.println(ret);

        responseEntity = restTemplate.getForEntity("http://nacos-provider/getInt?id=10",Integer.class,params);
        ret = responseEntity.getBody();
        System.out.println(ret);
    }

    @Test
    public void getBox(){
        Map<String,Object> params = new HashMap<>();
        params.put("name","小黑");
        params.put("id",100);

        ResponseEntity<String> responseEntity = restTemplate.getForEntity("http://nacos-provider/getBox?name={id}&id={id}",String.class,params);
        String ret = responseEntity.getBody();
        System.out.println(ret);

        responseEntity = restTemplate.getForEntity("http://nacos-provider/getBox?name=小黑&id=100",String.class,params);
        ret = responseEntity.getBody();
        System.out.println(ret);
    }

    @Test
    public void getReuqestParam(){
        Map<String,Object> params = new HashMap<>();
        params.put("rp",100);

        ResponseEntity<String> responseEntity = restTemplate.getForEntity("http://nacos-provider/getReuqestParam?rp={rp}",String.class,params);
        String ret = responseEntity.getBody();
        System.out.println(ret);

        responseEntity = restTemplate.getForEntity("http://nacos-provider/getReuqestParam?rp=100",String.class,params);

        ret = responseEntity.getBody();
        System.out.println(ret);

        String retObj = restTemplate.getForObject("http://nacos-provider/getReuqestParam?rp=100",String.class,params);

        System.out.println(retObj);

    }

    @Test
    public void getUser(){

        Map<String,Object> params = new HashMap<>();
        params.put("id",10);
        params.put("name","admin");
        params.put("password","123456");

        ResponseEntity<User> responseEntity = restTemplate.getForEntity("http://nacos-provider/getUser?id={id}&name={name}&password={password}",User.class,params);
        User user = responseEntity.getBody();

        System.out.println(user);

    }

    @Test
    public void getIds(){
        Map<String,Object> params = new HashMap<>();
        params.put("ids","1,2,3,4,5");

        ResponseEntity<ArrayList> responseEntity = restTemplate.getForEntity("http://nacos-provider/getIds?ids={ids}", ArrayList.class,params);
        List<Long> ids = responseEntity.getBody();

        System.out.println(ids);
    }

    @Test
    public void getMap(){
        Map<String,Object> params = new HashMap<>();
        params.put("name","小火锅");
        params.put("salary","3000");

        ResponseEntity<Map> responseEntity = restTemplate.getForEntity("http://nacos-provider/getMap?name={name}&salary={salary}", Map.class,params);
        Map ret = responseEntity.getBody();

        System.out.println(ret);
    }

    @Test
    public void getObj(){
        Map<String,Object> params = new HashMap<>();
        params.put("ik",300);

        ResponseEntity<Object> responseEntity = restTemplate.getForEntity("http://nacos-provider/getObj?ik={ik}", Object.class,params);
        Object ret = responseEntity.getBody();

        System.out.println(ret);
    }

    @Test
    public void getArr(){
        Map<String,Object> params = new HashMap<>();
        params.put("arr","1,2,3");

        ResponseEntity<String[]> responseEntity = restTemplate.getForEntity("http://nacos-provider/getArr?arr={arr}", String[].class,params);
        String[] ret = responseEntity.getBody();

        Arrays.stream(ret).forEach(System.out::println);
    }

    @Test
    public void getList(){
        Map<String,Object> params = new HashMap<>();
        params.put("names","1,2,3");

        ResponseEntity<ArrayList> responseEntity = restTemplate.getForEntity("http://nacos-provider/getList?names={names}", ArrayList.class,params);
        List<String> ret = responseEntity.getBody();

        ret.forEach(System.out::println);

    }

}
