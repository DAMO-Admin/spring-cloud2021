package com.nacosconsumer;

import com.nacosconsumer.entity.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@FeignClient(value = "nacos-provider",contextId = "DataPostRequestService")
public interface DataPostRequestService {

    @PostMapping("/postInt")
    int postInt(@RequestParam("id") int id);

    @PostMapping("/postBox")
    String postBox(@RequestParam("name") String name,@RequestParam("id") Integer id);

    @PostMapping("/postUser")
    User postUser(@RequestBody User user);

    @PostMapping("/postUserNoBody")
    User postUserNoBody(User user);

    @PostMapping("/postUser2")
    User postUser2(@RequestBody User user);

    @PostMapping("/postIds")
    List<Integer> postIds(@RequestBody List<Integer> ids);

    @PostMapping("/postUsers")
    List<User> postUsers(@RequestBody List<User> users);

    @PostMapping("/postMap")
    Map<String,Object> postMap(@RequestBody Map<String,Object> map);

}
