package com.nacosconsumer;

import com.nacosconsumer.entity.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@FeignClient(value = "nacos-provider",contextId = "DataGetRequestService")
public interface DataGetRequestService {

    @RequestMapping("/getInt")
    int getInt(@RequestParam("id") int id);

    @RequestMapping("/getBox")
    String getBox(@RequestParam("name") String name,@RequestParam("id") Integer id);

    @RequestMapping("/getReuqestParam")
    String getReuqestParam(@RequestParam("rp") Integer id);

    @RequestMapping("/getUser")
    User getUser(@SpringQueryMap User user);

    @RequestMapping("/getIds")
    List<Long> getIds(@RequestParam List<Long> ids);

    @RequestMapping("/getMap")
    Map<String,Object> getMap(@RequestParam Map<String,Object> map);

    @RequestMapping("/getObj")
    Object getObj(@RequestParam(name = "ik",required = true,defaultValue = "500") Integer id);

    @RequestMapping("/getArr")
    String[] getArr(@RequestParam String[] arr);

    @RequestMapping("/getList")
    List<String> getList(@RequestParam List<String> names);

}
