package com.nacosconsumer;

import com.nacosconsumer.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DataPostRequestServiceTest {

    @Autowired
    DataPostRequestService dataPostRequestService;

    @Test
    public void postInt(){
        Integer ret = dataPostRequestService.postInt(100);
        System.out.println(ret);
    }

    @Test
    public void postBox(){
        String ret = dataPostRequestService.postBox("小火锅",100);
        System.out.println(ret);
    }

    @Test
    public void postUser(){
        User user = new User();
        user.setId(1L);
        user.setName("周杰伦");
        user.setPassword("123456");

        User ret = dataPostRequestService.postUser(user);
        System.out.println(ret);
    }

    /**
     * 不支持
     */
    @Test
    public void postUserNoBody(){
        User user = new User();
        user.setId(1L);
        user.setName("周杰伦");
        user.setPassword("123456");

        User ret = dataPostRequestService.postUserNoBody(user);
        System.out.println(ret);
    }

    @Test
    public void postUser2(){
        User user = new User();
        user.setId(1L);
        user.setName("周杰伦");
        user.setPassword("123456");

        User ret = dataPostRequestService.postUser2(user);
        System.out.println(ret);
    }

    @Test
    public void postIds(){
        List<Integer> ids = new ArrayList<>();
        ids.add(1);
        ids.add(2);
        ids.add(3);

        List<Integer> ret = dataPostRequestService.postIds(ids);

        System.out.println(ret);

    }

    @Test
    public void postUsers(){
        User user = new User();
        user.setId(1L);
        user.setName("周杰伦");
        user.setPassword("123456");

        List<User> users = new ArrayList<>();
        users.add(user);

        List<User> ret = dataPostRequestService.postUsers(users);
        System.out.println(ret);
    }

    @Test
    public void postMap(){
        Map<String,Object> params = new HashMap<>();
        params.put("id",100);
        params.put("name","小火锅");
        params.put("salary",3000);

        Map<String,Object> ret = dataPostRequestService.postMap(params);

        System.out.println(ret);

    }

}
