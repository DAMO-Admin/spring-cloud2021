package com.nacosconsumer;

import com.nacosconsumer.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DataGetRequestServiceTests {

    @Autowired
    DataGetRequestService dataGetRequestService;

    @Test
    public void getInt(){
        int ret = dataGetRequestService.getInt(10);
        System.out.println(ret);
    }

    @Test
    public void getBox(){
        String ret = dataGetRequestService.getBox("admin",10);
        System.out.println(ret);
    }

    @Test
    public void getReuqestParam(){
        String ret = dataGetRequestService.getReuqestParam(10);
        System.out.println(ret);
    }

    @Test
    public void getUser(){
        User user = new User();
        user.setId(1L);
        user.setName("小黑");
        user.setPassword("123456");
        User ret = dataGetRequestService.getUser(user);
        System.out.println(ret);
    }

    @Test
    public void getIds(){
        List<Long> ids = new ArrayList<>();
        ids.add(1L);
        ids.add(2L);
        ids.add(3L);

        List<Long> ret = dataGetRequestService.getIds(ids);
        System.out.println(ret);

    }

    @Test
    public void getMap(){
        Map<String,Object> params = new HashMap<>();
        params.put("name","小火锅");
        params.put("id",100);
        params.put("hobby","轮滑、摩托、越野");

        Map<String,Object> ret = dataGetRequestService.getMap(params);
        System.out.println(ret);
    }

    @Test
    public void getObj(){
        Object ret = dataGetRequestService.getObj(100);
        System.out.println(ret);
    }

    @Test
    public void getArr(){
        String[] arr = {"1","小辣条","3000"};
        String[] ret = dataGetRequestService.getArr(arr);
        Arrays.stream(ret).forEach(System.out::println);
    }

    @Test
    public void getList(){
        List<String> list = Arrays.asList("100","小辣条","3000");
        List<String> ret = dataGetRequestService.getList(list);

        ret.forEach(System.out::println);
    }

}
