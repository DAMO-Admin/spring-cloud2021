package com.nacosconsumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class ConsumerController {
    @Autowired
    RestTemplate restTemplate;

    @GetMapping(value = "/hi-rest")
    public String hi(@RequestParam String name) {
        return restTemplate.getForObject("http://nacos-provider/hi?name="+name,String.class);
    }
}
