package com.nacosribbon;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RequestMapping("/goods")
@RestController
public class GoodsController {
    @Autowired
    RestTemplate restTemplate;

    @GetMapping(value = "/goodsName")
    public String goodsName(@RequestParam String name) {
        return restTemplate.getForObject("http://nacos-provider/hi?name="+name,String.class);
    }
}
